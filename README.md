# Magic Prompt Denis Demoineret
## _The best magic prompt from the Master DI_

[![N|Solid](https://www.ensitech.eu/fichiers/2019/12/ensitech-1024x363.png)]()


The magic prompt is a program that allow you to use some new command when you are registered.
- help :  Displays the available commands you can use.
- ls: Lists visible and hidden files and folders.
- rm: Deletes a file.
- rmd or rmdir: Deletes a directory.
- about: Provides a description of your program.
- version or --v or vers: Displays the version of your prompt.
- age: Asks for your age and tells you if you are a minor or an adult.
- quit: Allows you to exit the prompt.
- profil: Displays all information about yourself, including First Name, Last Name, Age, and Email.
- passw: Allows you to change the password with a confirmation request.
- cd: Navigates to a folder you've created or goes back to a previous folder.
- pwd: Shows the current working directory.
- hour: Provides the current time.
- *: Indicates an unknown command.
- httpget: Lets you download the HTML source code of a web page and save it to a specific file. Your prompt should ask for the file's name.
- smtp: Allows you to send an email with an address, subject, and email body.
- open: Opens a file directly in the VIM editor even if the file doesn't exist.
- login : allow you to login yourself, to create an account 
## obligation

- you have to best registered to use all function.
