#!/bin/bash
login(){
# Crée un tableau principal pour stocker les informations des utilisateurs
if [ -z "${listUser[@]}" ]; then
    listUser=()
    listUser+=("Denis78 password1 Denis Demoineret 21 email1")
    listUser+=("Loic78 password2 Loic Lepie 21 email2")
    listUser+=("Andrews78 password3 Andrews Agricole 21 email3")
fi

if [ "$connecte" = "connecté" ]; then
    echo "Vous êtes déjà connecté $new_user"
else
    echo "Avez-vous un compte ? y/n"
    read reponse4
    if [ "$reponse4" = "y" ]; then
        echo "Veuillez vous connecter, saisissez votre login :"
        read login
        echo "Saisissez votre mot de passe :"
        read mdp

        connecte="non connecté"

        # Parcours des utilisateurs pour vérifier si les informations de connexion correspondent
        for utilisateur_info in "${listUser[@]}"; do
            utilisateur=($utilisateur_info)
            utilisateur_login="${utilisateur[0]}"
            utilisateur_mdp="${utilisateur[1]}"
            
            if [ "$login" = "$utilisateur_login" ] && [ "$mdp" = "$utilisateur_mdp" ]; then
                connecte="connecté"
                utilisateur_prenom="${utilisateur[2]}"
                utilisateur_nom="${utilisateur[3]}"
                utilisateur_age="${utilisateur[4]}"
                utilisateur_mail="${utilisateur[5]}"
                echo "Connexion réussie en tant que $utilisateur_login."
                break
            fi
        done

        if [ "$connecte" = "non connecté" ]; then
            echo "Échec de la connexion. Vérifiez votre login et votre mot de passe."
        else 
            echo "Vous êtes connecté"
        fi
    else
        echo "voulez-vous créer un compte ? y/n"
        read reponse5
        if [ "$reponse5" = "y" ]; then
            echo "Rentrez votre login : "
            read loginNew
            echo "Rentrez votre mdp : "
            read mdpNew
            echo "Rentrez votre prénom : "
            read prenomNew
            echo "Rentrez votre nom : "
            read nomNew
            echo "Rentrez votre âge : "
            read ageNew
            echo "Rentrez votre mail : "
            read emailNew 
            utilisateur=("$loginNew $mdpNew $prenomNew $nomNew $ageNew $emailNew")
            utilisateur_login="$loginNew"
            utilisateur_mdp="$mdpNew"
            utilisateur_prenom="$prenomNew"
            utilisateur_nom="$nomNew"
            utilisateur_age="$ageNew"
            utilisateur_mail="$emailNew"
            listUser+=("$utilisateur_login $utilisateur_mdp $utilisateur_prenom $utilisateur_nom $utilisateur_age $utilisateur_mail")
            echo "Votre compte a été ajouté et vous êtes connecté $utilisateur_login"
            connecte="connecté"
        fi
    fi
fi
}