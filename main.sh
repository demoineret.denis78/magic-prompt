#!/bin/bash
source quit.sh
source help.sh
source ls.sh
source rm.sh
source rmdir.sh
source about.sh
source version.sh
source age.sh
source profil.sh
source open.sh
source pwd.sh
source hour.sh
source cd.sh
source login.sh
source logout.sh
source passw.sh
source httpget.sh
source smtp.sh
cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    aide | help ) help;;
    ls ) ls $2 $3;;
    rm ) rm $2 $3;;
    rmdir | rmd ) rmdir $2 $3;;
    about ) about;;
    version | --v | vers ) version;;
    age ) age;;
    profil ) profil;;
    open ) open;;
    pwd ) pwd;;
    hour ) hour;;
    cd ) cd $2;;
    login ) login;;
    logout ) logout;;
    passw ) passw;;
    httpget ) httpget;;
    smtp ) smtp;;
    * ) echo "Commande inconnue : $cmd";;

  esac
}

main() {
  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mXzen\033[m ~ ☠️ ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
