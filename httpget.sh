#!/bin/bash

httpget() {
    if [ "$connecte" = "connecté" ]; then
        echo "Entrez l'URL de la page web que vous souhaitez télécharger :"
        read url
        filename="html_$(date +'%Y%m%d%H%M%S').txt"

        #wget télécharge le contenu de la page et -O choisi la direction où il sera mis.
        wget "$url" -O "$filename"

        #$? c'est le code retour de la requete d'avant / -eq = =
        if [ $? -eq 0 ]; then
            echo "Téléchargement terminé avec succès. Le fichier est enregistré sous le nom : $filename"
        else
            echo "Erreur lors du téléchargement de la page web."
        fi

    else
        echo "veuillez vous connecter d'abord"
        login
    fi
}

