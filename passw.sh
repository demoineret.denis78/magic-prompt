#!/bin/bash
source login.sh
passw() {
    if [ "$connecte" = "connecté" ]; then
        echo "Saisissez votre nouveau mot de passe :"
        read nouveau_mdp
        echo "êtes-vous sûr de vouloir $nouveau_mdp comme nouveau mot de passe ? y/n"
        read reponse6
        if [ "$reponse6" = "y" ]; then
            for index in "${!listUser[@]}"; do
                utilisateur_info="${listUser[$index]}"
                utilisateur=($utilisateur_info)
                loginUtilisateur="${utilisateur[0]}"
                mdpUtilisateur="${utilisateur[0]}"
                if [ "$loginUtilisateur" = "$utilisateur_login" ] && [ "$mdpUtilisateur" = "$utilisateur_mdp" ]; then
                    utilisateur_prenom="${utilisateur[2]}"
                    utilisateur_nom="${utilisateur[3]}"
                    utilisateur_age="${utilisateur[4]}"
                    utilisateur_mail="${utilisateur[5]}"
                    listUser[$index]="$utilisateur_login $nouveau_mdp ${utilisateur[2]} ${utilisateur[3]} ${utilisateur[4]} ${utilisateur[5]}"
                    utilisateur_mdp="$nouveau_mdp"
                    echo "Mot de passe de l'utilisateur $utilisateur_login mis à jour avec succès."
                    break
                fi
            done
            #affiche la liste des users pour montrer que ça marche bien
            for utilisateur_info in "${listUser[@]}"; do
                utilisateur=($utilisateur_info)
                utilisateur_login="${utilisateur[0]}"
                utilisateur_prenom="${utilisateur[2]}"
                echo "$utilisateur $utilisateur_info"
            done
        fi
    else
        echo "veuillez vous connecter d'abord"
        login
    fi
}
